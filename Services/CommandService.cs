using System;
using hotel_management_system.AppExceptions;
using hotel_management_system.Models;
using hotel_management_system.Services.Interfaces;

namespace hotel_management_system.Services
{
    public class CommandService : ICommandService
    {
        private IHotelService _hotelService;
        public CommandService()
        {
            _hotelService = new HotelService();
        }

        public void BookByFloor(string[] parameters)
        {
            try
            {
                int floor;
                string bookName;
                int bookAge;
                Room[] roomsBooked;

                if (parameters.Length != 3) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out floor) == false) throw new InvalidParameterFloorException();
                if (string.IsNullOrEmpty(parameters[1]) == true) throw new InvalidParameterBookNameException();
                if (int.TryParse(parameters[2], out bookAge) == false) throw new InvalidParameterBookAgeException();

                bookName = parameters[1];

                roomsBooked = _hotelService.BookByFloor(floor, bookName, bookAge);
                if (roomsBooked != null && roomsBooked.Length > 0)
                {
                    Console.WriteLine($"Room {String.Join(", ", roomsBooked.Select(r => r.RoomNumber))} are booked with keycard number {String.Join(", ", roomsBooked.Select(r => r.KeyCardNumber))}.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void BookRoom(string[] parameters)
        {
            try
            {
                int roomNumber;
                string bookName;
                int bookAge;
                Room roomBooked;

                if (parameters.Length != 3) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out roomNumber) == false) throw new InvalidParameterRoomNumberException();
                if (string.IsNullOrEmpty(parameters[1]) == true) throw new InvalidParameterBookNameException();
                if (int.TryParse(parameters[2], out bookAge) == false) throw new InvalidParameterBookAgeException();

                bookName = parameters[1];

                roomBooked = _hotelService.BookRoom(roomNumber, bookName, bookAge);
                Console.WriteLine($"Room {roomNumber} is booked by {bookName} with keycard number {roomBooked.KeyCardNumber}.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Checkout(string[] parameters)
        {
            try
            {
                Room roomCheckedOut;
                int keyCardNumber;
                string bookName;

                if (parameters.Length != 2) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out keyCardNumber) == false) throw new InvalidParameterKeyCardNumberException();
                if (string.IsNullOrEmpty(parameters[1]) == true) throw new InvalidParameterBookNameException();

                bookName = parameters[1];

                roomCheckedOut = _hotelService.Checkout(keyCardNumber, bookName);
                Console.WriteLine($"Room {roomCheckedOut.RoomNumber} is checkout.");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void CheckoutByFloor(string[] parameters)
        {
            try
            {
                int floor;
                Room[] roomsCheckedOut;
                if (parameters.Length != 1) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out floor) == false) throw new InvalidParameterFloorException();

                roomsCheckedOut = _hotelService.CheckoutByFloor(floor);
                if (roomsCheckedOut != null && roomsCheckedOut.Length > 0)
                {
                    Console.WriteLine($"Room {String.Join(", ", roomsCheckedOut.Select(r => r.RoomNumber))} are checkout.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void CreateHotel(string[] parameters)
        {
            try
            {
                int floor;
                int roomPerFloor;

                if (parameters.Length != 2) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out floor) == false) throw new InvalidParameterFloorException();
                if (int.TryParse(parameters[1], out roomPerFloor) == false) throw new InvalidParameterRommPerFloorException();

                if (_hotelService.CreateHotel(floor, roomPerFloor))
                {
                    Console.WriteLine($"Hotel created with {floor} floor(s), {roomPerFloor} room(s) per floor.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ListAllGuests()
        {
            try
            {
                string[] allGuest = _hotelService.GetAllGuests();
                Console.WriteLine(String.Join(", ", allGuest));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ListAvailableRooms()
        {
            try
            {
                Room[] availableRooms = _hotelService.GetAvailableRooms();
                Console.WriteLine(String.Join(", ", availableRooms.Select(r => r.RoomNumber).ToArray()));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void GetGuestsByRoom(string[] parameters)
        {
            try
            {
                int roomNumber;
                if (parameters.Length != 1) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out roomNumber) == false) throw new InvalidParameterRoomNumberException();

                string guest = _hotelService.GetGuestsByRoom(roomNumber);
                Console.WriteLine(guest);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void ListGuestsByAgeRange(string[] parameters)
        {
            try
            {
                string comparator;
                int ageRange;
                if (parameters.Length != 2) throw new InvalidParameterException();
                if (string.IsNullOrEmpty(parameters[0]) == true) throw new InvalidParameterComparatorException();
                if (int.TryParse(parameters[1], out ageRange) == false) throw new InvalidParameterBookAgeException();

                comparator = parameters[0];
                string[] allGuestByAge = _hotelService.GetGuestsByAgeRange(comparator, ageRange);
                Console.WriteLine(String.Join(", ", allGuestByAge));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ListGuestsByFloor(string[] parameters)
        {
            try
            {
                int floor;
                if (parameters.Length != 1) throw new InvalidParameterException();
                if (int.TryParse(parameters[0], out floor) == false) throw new InvalidParameterFloorException();

                string[] allGuestByFloor = _hotelService.GetGuestsByFloor(floor);
                Console.WriteLine(String.Join(", ", allGuestByFloor));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
