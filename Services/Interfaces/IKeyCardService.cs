using System;
using hotel_management_system.Models;

namespace hotel_management_system.Services.Interfaces
{
    public interface IKeyCardService
    {
        public void CreateKeyCard(int totalKeyCard);
        public KeyCard? GetEmptyKeyCard();
        public void CheckoutKeyCard(int keyCardNumber);
    }
}
