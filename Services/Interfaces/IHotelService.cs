using System;
using hotel_management_system.Models;

namespace hotel_management_system.Services.Interfaces
{
    public interface IHotelService
    {
        public bool CreateHotel(int floor, int roomPerFloor);
        public Room BookRoom(int roomNumber, string bookName, int bookAge);
        public Room[] BookByFloor(int floor, string bookName, int bookAge);
        public Room Checkout(int keyCardNumber, string bookName);
        public Room[] CheckoutByFloor(int floor);
        public Room[] GetAvailableRooms();
        public string[] GetAllGuests();
        public string GetGuestsByRoom(int roomNumber);
        public string[] GetGuestsByAgeRange(string comparator, int age);
        public string[] GetGuestsByFloor(int floor);

    }
}
