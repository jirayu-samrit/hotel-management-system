using System;
using hotel_management_system.Models;

namespace hotel_management_system.Services.Interfaces
{
    public interface ICommandService
    {
        public void CreateHotel(string[] parameters);
        public void BookRoom(string[] parameters);
        public void BookByFloor(string[] parameters);
        public void Checkout(string[] parameters);
        public void CheckoutByFloor(string[] parameters);
        public void ListGuestsByAgeRange(string[] parameters);
        public void ListGuestsByFloor(string[] parameters);
        public void ListAvailableRooms();
        public void ListAllGuests();
        public void GetGuestsByRoom(string[] parameters);
    }
}
