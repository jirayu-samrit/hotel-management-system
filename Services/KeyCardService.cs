using System;
using hotel_management_system.Models;
using hotel_management_system.Services.Interfaces;

namespace hotel_management_system.Services
{
    public class KeyCardService : IKeyCardService
    {
        private KeyCard[]? _keyCards = null;

        private void CheckCreateKeyCard()
        {
            if (_keyCards == null) throw new Exception("Key card not create.");
        }
        public void CheckoutKeyCard(int keyCardNumber)
        {
            CheckCreateKeyCard();
            KeyCard? keyCard= _keyCards.FirstOrDefault(k => k.KeyCardNumber == keyCardNumber);
            if(keyCard==null) throw new Exception("Key card not found.");

            keyCard.Clear();
        }

        public void CreateKeyCard(int totalKeyCard)
        {
            if (_keyCards != null) throw new Exception("Key card has already Created.");
            _keyCards = new KeyCard[totalKeyCard];
            for (int k = 0; k < totalKeyCard; k++)
            {
                _keyCards[k] = new KeyCard(k + 1);
            }
        }

        public KeyCard? GetEmptyKeyCard()
        {
            CheckCreateKeyCard();
            return _keyCards.FirstOrDefault(k => k.RoomNumber == -1);
        }
    }
}
