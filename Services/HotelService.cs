using System;
using hotel_management_system.Models;
using hotel_management_system.Services.Interfaces;

namespace hotel_management_system.Services
{
    public class HotelService : IHotelService
    {
        private IKeyCardService _keyCardService;
        private List<Room>? _rooms = null;
        public HotelService()
        {
            _keyCardService = new KeyCardService();
        }
        private void CheckCreateHotel()
        {
            if (_rooms == null) throw new Exception("Hotel not created.");
        }
        private void CheckAvailableRoom()
        {
            if (_rooms.Any(r => r.KeyCardNumber == -1) == false)
            {
                throw new Exception("Hotel is fully booked.");
            }
        }

        public bool CreateHotel(int floor, int roomPerFloor)
        {
            if (_rooms != null) throw new Exception("Hotel has already created.");
            _rooms = new List<Room>();
            for (int f = 1; f <= floor; f++)
            {
                for (int r = 1; r <= roomPerFloor; r++)
                {
                    Room newRoom = new Room(
                       roomNumber: Convert.ToInt32($"{f}{r.ToString().PadLeft(2, '0')}"),
                       floor: f);
                    _rooms.Add(newRoom);
                }
            }
            _keyCardService.CreateKeyCard(floor * roomPerFloor);
            return true;
        }

        public Room[] BookByFloor(int floor, string bookName, int bookAge)
        {
            CheckCreateHotel();
            CheckAvailableRoom();

            if (_rooms.Any(r => r.Floor == floor && string.IsNullOrEmpty(r.BookName) == false && r.BookName != bookName))
            {
                throw new Exception($"Cannot book floor {floor} for {bookName}.");
            }
            List<Room> roomFloors = _rooms.FindAll(r => r.Floor == floor && r.KeyCardNumber == -1);
            if (roomFloors == null || roomFloors.Count == 0)
            {
                throw new Exception($"No Available Rooms on floor {floor}");
            }

            foreach (Room room in roomFloors)
            {
                BookRoom(room.RoomNumber, bookName, bookAge);
            }

            return roomFloors.ToArray();
        }

        public Room BookRoom(int roomNumber, string bookName, int bookAge)
        {
            CheckCreateHotel();
            CheckAvailableRoom();


            Room room = _rooms.Find(r => r.RoomNumber == roomNumber);

            if (room.KeyCardNumber != -1)
            {
                throw new Exception($"Cannot book room {roomNumber} for {bookName}, The room is currently booked by {room.BookName}.");
            }
            KeyCard? keyCard = _keyCardService.GetEmptyKeyCard();
            if (keyCard == null) throw new Exception($"No Available Key Card.");

            room.KeyCardNumber = keyCard.KeyCardNumber;
            room.BookName = bookName;
            room.BookAge = bookAge;

            keyCard.RoomNumber = roomNumber;
            return room;
        }

        public Room Checkout(int keyCardNumber, string bookName)
        {
            CheckCreateHotel();

            Room? room = _rooms.Find(r => r.KeyCardNumber == keyCardNumber);
            if (room == null) throw new Exception("This keycard never used.");

            if (room.BookName != bookName)
            {
                throw new Exception($"Only {room.BookName} can checkout with keycard number {keyCardNumber}.");
            }

            Room checkout = new Room(
                roomNumber: room.RoomNumber,
                floor: room.Floor
            );
            _keyCardService.CheckoutKeyCard(keyCardNumber);
            room.Clear();

            return checkout;
        }

        public Room[] CheckoutByFloor(int floor)
        {
            CheckCreateHotel();
            
            List<Room> roomFloors = _rooms.FindAll(r => r.Floor == floor && r.KeyCardNumber != -1);
            if (roomFloors == null || roomFloors.Count == 0)
            {
                throw new Exception($"No Guest booked on floor {floor}");
            }
            foreach (Room room in roomFloors)
            {
                Checkout(room.KeyCardNumber, room.BookName);
            }

            return roomFloors.ToArray();
        }

        public string[] GetAllGuests()
        {
            CheckCreateHotel();
            List<Room> rooms = _rooms.FindAll(r => r.KeyCardNumber != -1);
            if (rooms == null || rooms.Count == 0) throw new Exception($"No guest hotel.");
            return rooms.OrderBy(r => r.KeyCardNumber).Select(g => g.BookName).ToArray();
        }

        public Room[] GetAvailableRooms()
        {
            CheckCreateHotel();

            return _rooms.FindAll(r => string.IsNullOrEmpty(r.BookName)).ToArray();
        }

        public string[] GetGuestsByAgeRange(string comparator, int age)
        {
            CheckCreateHotel();
            List<Room>? rooms = null;
            switch (comparator)
            {
                case ">":
                    rooms = _rooms.FindAll(r => r.KeyCardNumber != -1
                                            && r.BookAge > age);
                    break;
                case "<":
                    rooms = _rooms.FindAll(r => r.KeyCardNumber != -1
                                            && r.BookAge < age);
                    break;
                case "<=":
                    rooms = _rooms.FindAll(r => r.KeyCardNumber != -1
                                            && r.BookAge <= age);
                    break;
                case ">=":
                    rooms = _rooms.FindAll(r => r.KeyCardNumber != -1
                                            && r.BookAge >= age);
                    break;
                case "==":
                    rooms = _rooms.FindAll(r => r.KeyCardNumber != -1
                                            && r.BookAge == age);
                    break;
                case "!=":
                    rooms = _rooms.FindAll(r => r.KeyCardNumber != -1
                                            && r.BookAge != age);
                    break;
                default:
                    break;
            }

            if (rooms == null || rooms.Count == 0) throw new Exception($"No guest in age range {comparator} - {age}.");
            return rooms.OrderBy(r => r.KeyCardNumber).Select(g => g.BookName).ToArray();
        }

        public string[] GetGuestsByFloor(int floor)
        {
            CheckCreateHotel();
            List<Room> rooms = _rooms.FindAll(r => r.KeyCardNumber != -1 && r.Floor == floor);
            if (rooms == null || rooms.Count == 0) throw new Exception($"No guest in floor {floor}");
            return rooms.OrderBy(r => r.KeyCardNumber).Select(g => g.BookName).ToArray();
        }

        public string GetGuestsByRoom(int roomNumber)
        {
            CheckCreateHotel();
            Room? room = _rooms.Find(r => r.RoomNumber == roomNumber);
            if (room == null) throw new Exception($"Room {roomNumber} not found.");
            if (room.KeyCardNumber == -1) throw new Exception($"No guest in room {roomNumber}.");
            return room.BookName;
        }
    }
}
