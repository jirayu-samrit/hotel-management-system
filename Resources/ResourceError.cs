using System;

namespace hotel_management_system.Resources
{
    public static class ResourceError
    {
        public static string InvalidParameter = "Invalid Parameter";
        public static string InvalidParameterFloor = "Invalid Parameter Floor";
        public static string InvalidParameterRoomPerFloor = "Invalid Parameter Room Per Floor";
        public static string InvalidParameterRoomNumber = "Invalid Parameter Room Number";
        public static string InvalidParameterKeyCardNumber = "Invalid Parameter Key Card Number";
        public static string InvalidParameterBookName = "Invalid Parameter Book Name";
        public static string InvalidParameterBookAge = "Invalid Parameter Book Age";
        public static string InvalidParameterAgeComparator = "Invalid Parameter Age Comparator";
    }
}
