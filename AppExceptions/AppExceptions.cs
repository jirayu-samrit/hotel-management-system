using System;
using hotel_management_system.Resources;

namespace hotel_management_system.AppExceptions
{
    public class InvalidParameterException : Exception
    {
        public InvalidParameterException() : base(ResourceError.InvalidParameter)
        {
        }

        public InvalidParameterException(string message)
            : base(message)
        {
        }

        public InvalidParameterException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterFloorException : Exception
    {
        public InvalidParameterFloorException() : base(ResourceError.InvalidParameterFloor)
        {
        }

        public InvalidParameterFloorException(string message)
            : base(message)
        {
        }

        public InvalidParameterFloorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterRommPerFloorException : Exception
    {
        public InvalidParameterRommPerFloorException() : base(ResourceError.InvalidParameterRoomPerFloor)
        {
        }

        public InvalidParameterRommPerFloorException(string message)
            : base(message)
        {
        }

        public InvalidParameterRommPerFloorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterBookNameException : Exception
    {
        public InvalidParameterBookNameException() : base(ResourceError.InvalidParameterBookName)
        {
        }

        public InvalidParameterBookNameException(string message)
            : base(message)
        {
        }

        public InvalidParameterBookNameException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterBookAgeException : Exception
    {
        public InvalidParameterBookAgeException() : base(ResourceError.InvalidParameterBookAge)
        {
        }

        public InvalidParameterBookAgeException(string message)
            : base(message)
        {
        }

        public InvalidParameterBookAgeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterRoomNumberException : Exception
    {
        public InvalidParameterRoomNumberException() : base(ResourceError.InvalidParameterRoomNumber)
        {
        }

        public InvalidParameterRoomNumberException(string message)
            : base(message)
        {
        }

        public InvalidParameterRoomNumberException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterKeyCardNumberException : Exception
    {
        public InvalidParameterKeyCardNumberException() : base(ResourceError.InvalidParameterKeyCardNumber)
        {
        }

        public InvalidParameterKeyCardNumberException(string message)
            : base(message)
        {
        }

        public InvalidParameterKeyCardNumberException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class InvalidParameterComparatorException : Exception
    {
        public InvalidParameterComparatorException() : base(ResourceError.InvalidParameterAgeComparator)
        {
        }

        public InvalidParameterComparatorException(string message)
            : base(message)
        {
        }

        public InvalidParameterComparatorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
