﻿using hotel_management_system.Models;
using hotel_management_system.Services;
using hotel_management_system.Services.Interfaces;

namespace hotel_management_system
{
    public static class Program
    {
        public static Command[] GetCommandsFromFile(string filename)
        {
            string fileContent = File.ReadAllText(filename);

            return fileContent
                .Split(Environment.NewLine)
                .Select(line => line.Trim())
                .Where(line => !string.IsNullOrEmpty(line))
                .Select(line => line.Split(' '))
                .Select(parameters => new Command(parameters[0], parameters.Skip(1).ToArray()))
                .ToArray();
        }

        public static void Main(string[] args)
        {
            const string fileName = "input.txt";
            Command[] commands = GetCommandsFromFile(fileName);
            ICommandService commandService = new CommandService();
            foreach (Command command in commands)
            {
                // Console.WriteLine($"Command: {command.Name}\tParams: {String.Join(' ', command.Parameters)} ");
                switch (command.Name)
                {
                    case "create_hotel":
                        commandService.CreateHotel(command.Parameters);
                        break;
                    case "book":
                        commandService.BookRoom(command.Parameters);
                        break;
                    case "book_by_floor":
                        commandService.BookByFloor(command.Parameters);
                        break;
                    case "checkout":
                        commandService.Checkout(command.Parameters);
                        break;
                    case "checkout_guest_by_floor":
                        commandService.CheckoutByFloor(command.Parameters);
                        break;
                    case "list_available_rooms":
                        commandService.ListAvailableRooms();
                        break;
                    case "list_guest":
                        commandService.ListAllGuests();
                        break;
                    case "get_guest_in_room":
                        commandService.GetGuestsByRoom(command.Parameters);
                        break;
                    case "list_guest_by_age":
                        commandService.ListGuestsByAgeRange(command.Parameters);
                        break;
                    case "list_guest_by_floor":
                        commandService.ListGuestsByFloor(command.Parameters);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}