using System;

namespace hotel_management_system.Models
{
    public class Room
    {

        public int RoomNumber { get; set; }
        public int Floor { get; set; }
        public string BookName { get; set; }
        public int BookAge { get; set; }
        public int KeyCardNumber { get; set; }
        public Room(int roomNumber, int floor, string bookName = "", int bookAge = -1, int keyCardNumber = -1)
        {
            RoomNumber = roomNumber;
            BookName = bookName;
            BookAge = bookAge;
            KeyCardNumber = keyCardNumber;
            Floor = floor;
        }

        public void Clear()
        {
            BookName = string.Empty;
            BookAge = -1;
            KeyCardNumber = -1;
        }
    }
}
