using System;
using System.ComponentModel.DataAnnotations;

namespace hotel_management_system.Models
{
    public class KeyCard
    {
        [Key]
        public int KeyCardNumber { get; set; }
        public int RoomNumber { get; set; } = -1;

        public KeyCard(int keyCardNumber)
        {
            KeyCardNumber = keyCardNumber;
        }
        public void Clear()
        {
            RoomNumber = -1;
        }
    }
}
