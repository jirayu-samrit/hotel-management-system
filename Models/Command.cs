using System;

namespace hotel_management_system.Models
{
    public class Command
    {
        public string Name { get; set; }
        public string[] Parameters { get; set; }

        public Command(string name, string[] parameters)
        {
            Name = name;
            Parameters = parameters;
        }
    }
}
